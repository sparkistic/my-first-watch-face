package com.sparkistic.myfirstwatchface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    public static final String COLOR_CHANGE_EVENT = "ColorChangeEvent";

    private RadioGroup radioGroup;
    private Button colorButton;
    private Map<String, RadioButton> radioButtons = new HashMap<>();
    private boolean isColorFromWatch = false;

    final BroadcastReceiver mColorMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String color = intent.getStringExtra("color");
            setSelectedColor(color);
        }
    };

    private void setSelectedColor(String color) {
        Log.d("MainActivity", "Received color from watch: " + color);
        isColorFromWatch = true;
        radioButtons.get(color).setChecked(true);
        setButtonBackground(color);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Initialize Radio Group and attach click handler */
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);

        for (int i=0; i<radioGroup.getChildCount(); i++) {
            RadioButton rb = (RadioButton) radioGroup.getChildAt(i);
            radioButtons.put(rb.getText().toString(), rb);
        }

        try {
            radioGroup.clearCheck();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Attach CheckedChangeListener to radio group */
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb != null && checkedId > -1 && rb.isChecked() && !isColorFromWatch) {
                    String colorMessage = rb.getText().toString();

                    setButtonBackground(colorMessage);

                    Toast.makeText(MainActivity.this, "Sending " + colorMessage, Toast.LENGTH_SHORT).show();
                    WearMessagingHelper.sendMessageToWatch(getApplicationContext(), colorMessage);
                }

                isColorFromWatch = false;
            }
        });


        /* Initialize color button and attach click handler */
        colorButton = (Button) findViewById(R.id.color_button);

        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchRandomColor();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mColorMessageReceiver,
                new IntentFilter(COLOR_CHANGE_EVENT));
    }

    private void fetchRandomColor() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("num", "3");
        params.put("min", "0");
        params.put("max", "255");
        params.put("col", "1");
        params.put("base", "16");
        params.put("format", "plain");
        params.put("rnd", "new");

        client.get("https://www.random.org/integers/", params, new TextHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String res) {
                        String lines[] = res.split("\\r?\\n");
                        String hexColor = "#" + lines[0] + lines[1] + lines[2];

                        setButtonBackground(hexColor);

                        Toast.makeText(MainActivity.this, "Sending " + hexColor, Toast.LENGTH_SHORT).show();
                        WearMessagingHelper.sendMessageToWatch(getApplicationContext(), hexColor);

                        radioGroup.clearCheck();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        System.out.println("something went wrong! " + res);
                    }
                }
        );
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mColorMessageReceiver);
        super.onDestroy();
    }

    private void setButtonBackground(String color) {
        int parsedColor = Color.parseColor(color);
        colorButton.setBackgroundColor(parsedColor);

        float lum = 1-(0.299f*Color.red(parsedColor) + 0.587f * Color.green(parsedColor) + 0.114f * Color.blue(parsedColor)) / 255f;
        if (lum < 0.5) {
            colorButton.setTextColor(Color.BLACK);
        } else {
            colorButton.setTextColor(Color.WHITE);
        }
    }
}

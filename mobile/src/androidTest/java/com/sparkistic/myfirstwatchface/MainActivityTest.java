package com.sparkistic.myfirstwatchface;

import android.graphics.drawable.ColorDrawable;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

/**
 * TODO - this is just the beginnings of a test class,
 * this is very much in-progress.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void testBlue() {

        onView(withId(R.id.radio_blue)).perform(ViewActions.click());

        MainActivity activity = mActivityRule.getActivity();
        Button colorButton = (Button) activity.findViewById(R.id.color_button);

        ColorDrawable background = (ColorDrawable) colorButton.getBackground();

        assertEquals("ff0000ff", Integer.toHexString(background.getColor()));
    }

    @Test
    public void testNavy() {

        onView(withId(R.id.radio_navy)).perform(ViewActions.click());

        MainActivity activity = mActivityRule.getActivity();
        Button colorButton = (Button) activity.findViewById(R.id.color_button);

        ColorDrawable background = (ColorDrawable) colorButton.getBackground();

        assertEquals("ff000080", Integer.toHexString(background.getColor()));
    }

}

package com.sparkistic.myfirstwatchface;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.List;

public class MobileMessagingHelper {
    private static GoogleApiClient mGoogleApiClient;

    public static void sendMessageToMobile(Context ctx, final String message) {

        if (mGoogleApiClient == null) {
            setupGoogleClient(ctx);
        }
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        new AsyncTask<Object, Void, Void>() {
            @Override
            protected Void doInBackground(Object... params) {
                try {
                    String state = (String) params[0];
                    GoogleApiClient googleApiClient = (GoogleApiClient) params[1];
                    List<Node> nodeList = getNodes();
                    for (Node node : nodeList) {
                        Wearable.MessageApi.sendMessage(
                                googleApiClient,
                                node.getId(),
                                state,
                                null
                        ).await();
                    }
                } catch (Throwable t) {
                }
                return null;
            }
        }.execute(message, mGoogleApiClient);
    }

    private static List<Node> getNodes() {
        List<Node> nodes = new ArrayList<>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }

    private static void setupGoogleClient(Context ctx) {
        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
//                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }

                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult result) {
                        //Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }


}

package com.sparkistic.myfirstwatchface;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

public class WearMessagingService extends WearableListenerService {

    private static final String TAG = "WearMessagingService";
    private static final String PACKAGE = "com.sparkistic.myfirstwatchface";
    public static final String BACKGROUND_COLOR = "com.sparkistic.myfirstwatchface.backgroundcolor";
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGoogleClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    private void setupGoogleClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                    }
                })
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onMessageReceived(final MessageEvent messageEvent) {
        Log.d(TAG, "Message received on wear: " + messageEvent.getPath());
        storeColor(messageEvent.getPath());
    }

    private void storeColor(String color) {
        SharedPreferences sharedPref = this.getSharedPreferences(PACKAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(BACKGROUND_COLOR, color);
        editor.apply();
    }

}
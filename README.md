# Welcome! #

Welcome to your first Android Wear Watch Face! This repository holds an incremental walk-through of how to get started creating your first watch face. See the presentation below for more information.

### Socal Code Camp 2016 - [Link to Presentation](https://docs.google.com/presentation/d/1C-J3LqPNDdnblPTGBYH2nCmZhEddh7OqAmME8P84ahI/edit?usp=sharing) ###

### Android Wear Documentation ###

* Start here (tutorials) - [Creating Watch Faces](https://developer.android.com/training/wearables/watch-faces/index.html)
* Watch face style guide - [Style guide](https://developer.android.com/design/wear/watchfaces.html)
* Dealing with round vs square and chin - [Addressing Common Issues](https://developer.android.com/training/wearables/watch-faces/issues.html)
* Canvas methods - [Canvas](https://developer.android.com/reference/android/graphics/Canvas.html)

### Other Resources ###

* Pairing emulators - [StackOverflow](http://stackoverflow.com/questions/25205888/pairing-android-and-wear-emulators)
* What's new in Android Wear 2.0 - [Google I/O 2016](https://www.youtube.com/watch?v=L_Z3gSXGsyI)
* LoopJ - [Main page](http://loopj.com/android-async-http/) - [Example setup](https://guides.codepath.com/android/Using-Android-Async-Http-Client)
* Join the Android Wear Developers group - [Google+](https://plus.google.com/communities/113381227473021565406)

### Sparkistic Watch Faces ###

* [Just A Minute](https://play.google.com/store/apps/details?id=com.sparkistic.justaminute&utm_source=bitbucket&utm_medium=social&utm_campaign=socalcodecamp)
* [Just A Minute - Pride Edition](https://play.google.com/store/apps/details?id=com.sparkistic.justaminutepride&utm_source=bitbucket&utm_medium=social&utm_campaign=socalcodecamp)
* [PhotoWear](https://play.google.com/store/apps/details?id=com.sparkistic.photowear&utm_source=bitbucket&utm_medium=social&utm_campaign=socalcodecamp)